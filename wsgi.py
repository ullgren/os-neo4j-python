from flask import Flask, render_template, Response
import json
import os
from queryservice import QueryService

application = Flask(__name__)
queryservice = QueryService(os.getenv('NEO4J_URI'), os.getenv("NEO4J_USER"), os.getenv("NEO4J_PASSWORD"))

@application.route("/docs")
def get_docs():
    return render_template('swaggerui.html')

@application.route("/api/places")
def hello():
    result = queryservice.get_countries()
    return Response(json.dumps(result), mimetype='application/json')


if __name__ == "__main__":
    application.run()
