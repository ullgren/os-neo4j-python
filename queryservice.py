from neo4j import GraphDatabase

class QueryService:
    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def __del__(self):
        self.close()

    def close(self):
        self.driver.close()

    def get_countries(self):
        with self.driver.session() as session:
            records = session.write_transaction(self._match_and_return_countries)
            countries = map(self.record_to_object, records)
            return list(countries)

    @staticmethod
    def _match_and_return_countries(tx):
        return list(tx.run("MATCH (c:Country) RETURN c.uuid AS uuid, c.name AS name LIMIT 10"))

    @staticmethod
    def record_to_object(r):
        return {
            "uuid": r["uuid"],
            "name": r["name"]
        }